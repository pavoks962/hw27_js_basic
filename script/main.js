"use strict"


$('.warm').click(function () {
    $('html, body').animate({
        scrollTop: $('.network').offset().top
    }, 1000);
});

$('.build').click(function () {
    $('html, body').animate({
        scrollTop: $('.machinery').offset().top
    }, 1000);
});

$('.another').click(function () {
    $('html, body').animate({
        scrollTop: $('.clients').offset().top
    }, 1000);
});

$(window).scroll(function () {
    if ($(this).scrollTop() > $(window).height()) {
        $('.js-button').css('display', 'block')
    } else {
        $('.js-button').css('display', 'none')
    }
});


$('.js-button').on("click", function (e) {
    var body = $("html, body");
    body.stop().animate({ scrollTop: 0 }, 500, "swing");
});

$('.js-button-slideToggle').click(function () {
    $('.network').slideToggle("slow");
});